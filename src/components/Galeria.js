import React from "react";
import { useState } from "react";
import { arteMexicanoList } from "../data/arteMexicano";
import Container from "react-bootstrap/Container";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Pagination from "react-bootstrap/Pagination";

export default function Gallery() {
  const [index, setIndex] = useState(0);
  const [showMore, setShowMore] = useState(true);

  let hasPrev = index > 0;
  let hasNext = index < arteMexicanoList.length - 1;

  function handlePrevClick() {
    if (hasPrev) {
      setIndex(index - 1);
    }
  }

  function handleNextClick() {
    if (hasNext) {
      setIndex(index + 1);
    }
  }

  function handleMoreClick() {
    setShowMore(!showMore);
  }

  let sculpture = arteMexicanoList[index];
  return (
    <Container>
      <Row>
        <Pagination size="md">
          <Pagination.Item onClick={handlePrevClick} disabled={!hasPrev}>
            Anterior
          </Pagination.Item>
          <Pagination.Item onClick={handleNextClick} disabled={!hasNext}>
            Siguiente
          </Pagination.Item>
        </Pagination>
        <h2>
          <i>{sculpture.name} </i>
          por {sculpture.artist}
        </h2>

        <h5>
          ({index + 1} de {arteMexicanoList.length})
        </h5>
        <Button variant="outline-info" onClick={handleMoreClick}>
          {showMore ? "Ocultar" : "Mostrar"} detalles
        </Button>
      </Row>
      <Row>
        <Col xs={7} md={6}>
          <Image
            src={sculpture.url}
            alt={sculpture.alt}
            style={{ padding: "10px", marginTop: "10px" }}
            thumbnail
          />
        </Col>
        <Col xs={5} md={6}>
          {showMore && (
            <p style={{ padding: "10px", marginTop: "10px" }}>
              {sculpture.description}
            </p>
          )}
        </Col>
      </Row>
    </Container>
  );
}
