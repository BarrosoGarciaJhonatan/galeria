import React, { useState, useRef } from "react";

export default function Autentication(props) {
  let [authMode, setAuthMode] = useState("signin");
  const email = useRef(null);
  const password = useRef(null);

  const changeAuthMode = () => {
    setAuthMode(authMode === "signin" ? "signup" : "signin");
  };
  const getUserData = () => {
    alert(
      `Usuario: ${email.current.value}, Contraseña: ${password.current.value}`
    );
  };

  if (authMode === "signin") {
    return (
      <div className="Auth-form-container">
        <form className="Auth-form">
          <div className="Auth-form-content">
            <h3 className="Auth-form-title">Iniciar sesión</h3>
            <div className="form-group mt-3">
              <label>Dirección de correo electrónico:</label>
              <input
                ref={email}
                type="email"
                className="form-control mt-1"
                placeholder="Ingrese su email"
              />
            </div>
            <div className="form-group mt-3">
              <label>Password</label>
              <input
                ref={password}
                type="password"
                className="form-control mt-1"
                placeholder="Enter password"
              />
            </div>
            <div className="d-grid gap-2 mt-3">
              <button
                type="submit"
                className="btn btn-primary"
                onClick={getUserData}
              >
                Acceder
              </button>
            </div>
            <div className="text-center">
              <span className="link-primary" onClick={changeAuthMode}>
                ¿No se ha registrado?
              </span>
              <p className="text-center mt-2">
                ¿Olvidó su <a href>contraseña?</a>
              </p>
            </div>
          </div>
        </form>
      </div>
    );
  }

  return (
    <div className="Auth-form-container">
      <form className="Auth-form">
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Formulario de registro</h3>
          <div className="text-center">
            ¿Ya está registrado?{" "}
            <span className="link-primary" onClick={changeAuthMode}>
              Iniciar sesión
            </span>
          </div>
          <div className="form-group mt-3">
            <label>Nombre completo:</label>
            <input
              type="text"
              className="form-control mt-1"
              placeholder="Josue Emanuel Barahona"
            />
          </div>
          <div className="form-group mt-3">
            <label>Dirección de correo electrónico:</label>
            <input
              type="email"
              className="form-control mt-1"
              placeholder="jack.emanuel@gmail.com"
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              className="form-control mt-1"
              placeholder="Password"
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Registrar
            </button>
          </div>
          <p className="text-center mt-2">
            ¿Olvidó su <a href>contraseña?</a>
          </p>
        </div>
      </form>
    </div>
  );
}
