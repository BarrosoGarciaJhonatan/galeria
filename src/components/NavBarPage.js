import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
export default function NavBarPage() {
  return (
    <Navbar bg="dark" data-bs-theme="dark">
      <Container>
        <Nav className="me-auto">
          <Navbar.Brand href="/home">Home</Navbar.Brand>
          <Navbar.Brand href="/galeria">Galería</Navbar.Brand>
        </Nav>
      </Container>
    </Navbar>
  );
}
