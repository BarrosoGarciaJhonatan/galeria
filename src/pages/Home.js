import React from "react";
import { Image } from "react-bootstrap";
import NavBarPage from "../components/NavBarPage";
export default function Home() {
  return (
    <>
      <NavBarPage />
      <div className="App-body">
        <h1>GALLERY HOME PAGE</h1>
        <Image
          src="https://pimponeti.com/wp-content/uploads/2023/05/logo-web-11.png"
          style={{ width: "50%" }}
        />
      </div>
    </>
  );
}
