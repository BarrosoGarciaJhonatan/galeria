import React from "react";
import Galeria from "../components/Galeria";
import NavBarPage from "../components/NavBarPage";
export default function Gallery() {
  return (
    <>
      <NavBarPage />
      <div className="App-body">
        <Galeria />
      </div>
    </>
  );
}
