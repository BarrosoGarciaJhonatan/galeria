import "./App.css";
import Home from "./pages/Home";
import Gallery from "./pages/Gallery";
import LoginPage from "./pages/LoginPage";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <div>
        <Router>
          <Routes>
            <Route path="/" element={<LoginPage />} />
            <Route path="/home" element={<Home />} />
            <Route path="/galeria" element={<Gallery />} />
          </Routes>
        </Router>
      </div>
    </div>
  );
}

export default App;
