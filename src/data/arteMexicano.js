export const arteMexicanoList = [
  {
    name: "Las dos Fridas",
    artist: "Frida Kahlo",
    description:
      "Las dos Fridas es un cuadro al óleo de Frida Kahlo pintado en 1939​. Esta obra constituye un autorretrato doble de la artista en la cual se duplica su imagen a manera de espejo, y tomada de la mano; pero con diferente vestimenta. Es un autorretrato surrealista y sorprendente que presenta a dos Frida Kahlos idénticas sentadas en un banco contra un fondo nublado y tormentoso. Cada Frida está vestida con ropa distinta, que simboliza diferentes aspectos de su identidad y herencia.",
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-26-800x445.webp",
    alt: "Es un testimonio del genio artístico de Frida Kahlo y su capacidad para transmitir sus pensamientos y emociones más íntimos a través de su arte.",
  },
  {
    name: "La noche estrellada",
    artist: "Vincent van Gogh",
    description:
      "La noche estrellada es una obra maestra que representa el cielo nocturno con turbulencia y movimiento. Van Gogh pintó esta pieza durante su estancia en un asilo en Saint-Rémy-de-Provence.",
    url: "https://hips.hearstapps.com/hmg-prod/images/starry-night-by-vincent-van-gogh-a-post-impressionist-news-photo-1645366088.jpg?crop=1.00xw:0.635xh;0.00160xw,0.337xh&resize=640:*",
    alt: "Imagen de La noche estrellada",
  },
  {
    name: "Mona Lisa",
    artist: "Leonardo da Vinci",
    description:
      "Mona Lisa, también conocida como La Gioconda, es un retrato renacentista conocido por la sonrisa enigmática de la mujer retratada. Es una de las pinturas más famosas del mundo.",
    url: "https://cloudfront-us-east-1.images.arcpublishing.com/copesa/7ARBKZBF3JDNLCJOS2BJEGT6WQ.jpg",
    alt: "Imagen de Mona Lisa",
  },
  {
    name: "El Gran Tenochtitlán",
    artist: "Diego Rivera",
    description:
      'El mural "El Gran Tenochtitlán" celebra la herencia prehispánica y la historia indígena de México. El mural sirve como recordatorio del rico tapiz cultural de México, anterior a la conquista española. Los murales de Diego Rivera a menudo llevaban mensajes políticos y sociales, y «La Gran Tenochtitlán» no es una excepción. A través de este mural, Rivera destacó la resiliencia de las culturas indígenas y sus contribuciones a la identidad mexicana. Al mismo tiempo, enfatizó la necesidad de unidad y orgullo nacional ante los desafíos modernos.',
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-21.webp",
    alt: "«La Gran Tenochtitlán» sigue siendo un importante símbolo cultural e histórico en México. Es un testimonio del compromiso de Diego Rivera de retratar la historia, la cultura y los problemas sociales de México a través de su arte.",
  },
  {
    name: "El hombre en la encrucijada",
    artist: "Diego Rivera",
    description:
      '"El hombre en la encrucijada" de Diego Rivera fue un mural innovador que abordó complejos temas sociales y políticos de su época. A pesar de su destrucción, sigue siendo una parte importante de la historia del arte y un símbolo de expresión y controversia artística.',
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-18.webp",
    alt: "El mural es un fresco a gran escala que mide aproximadamente 22 pies de alto y 63 pies de ancho. El mural es muy detallado y está repleto de diversos elementos simbólicos, figuras históricas y temas contemporáneos.",
  },
  {
    name: "El portador de Flores",
    artist: "Diego Rivera",
    description:
      '"El portador de flores" representa a un trabajador, comúnmente conocido como «cargador», que lleva un enorme ramo de flores en su espalda. El ramo de flores consta de flores vibrantes y coloridas, que incluyen girasoles, caléndulas y otras variedades comúnmente asociadas con la cultura mexicana.',
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-19.webp",
    alt: "El fondo del cuadro es relativamente sencillo, con tonos terrosos y un paisaje suave. Este fondo mínimo dirige la atención del espectador hacia el trabajador y las flores, resaltando los temas centrales de la obra de arte.",
  },
  {
    name: "Autorretrato con collar de espinas y colibrí",
    artist: "Frida Kahlo",
    description:
      'En "Autorretrato con collar de espinas y colibrí", Frida Kahlo se presenta de frente, mirando directamente al espectador con ojos intensos e inquebrantables. La pintura es un retrato en primer plano, que se centra principalmente en la parte superior del cuerpo y la cara.',
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-20.webp",
    alt: "La pintura resuena entre los espectadores y sirve como testimonio de la influencia duradera de Kahlo en el mundo del arte y su capacidad para utilizar sus experiencias personales para crear un arte profundamente conmovedor y universalmente identificable.",
  },
  {
    name: "La columna rota",
    artist: "Frida Kahlo",
    description:
      "Sigue siendo una de las obras más icónicas e influyentes de Kahlo, y sirve como testimonio de su influencia duradera en el mundo del arte y su capacidad para utilizar sus experiencias personales para crear un arte profundamente conmovedor y universalmente identificable.",
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-22.webp",
    alt: "En «La columna rota», Frida Kahlo se presenta como una figura desnuda de pie frente a un paisaje árido.",
  },
  {
    name: "Los Girasoles",
    artist: "Diego Rivera",
    description:
      "Inspirándose en la famosa serie de pinturas de girasoles de Vincent van Gogh, Diego Rivera creó su versión de «Los Girasoles» (Los Girasoles) en 1947. Esta vibrante obra de arte muestra el estilo único de Rivera y su interpretación del tema.",
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-23.webp",
    alt: "La fusión de Rivera de influencias artísticas europeas y mexicanas es evidente en esta notable pieza.",
  },
  {
    name: "Sueño de una tarde de domingo en el Parque Alameda Central",
    artist: "Diego Rivera",
    description:
      '"Sueño de una tarde de domingo en el Parque Central de la Alameda" celebra la identidad, la historia y la cultura mexicanas. Destaca el complejo tapiz de la sociedad mexicana, entretejiendo figuras históricas, símbolos folclóricos y gente común. La presencia de La Catrina subraya la idea de que la muerte es una parte siempre presente y aceptada de la cultura mexicana.',
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-24.webp",
    alt: "«Sueño de una tarde de domingo en Alameda Central Park» es un enorme mural al fresco que mide aproximadamente 15 metros (49 pies) de largo y unos 4 metros (13 pies) de alto.",
  },
  {
    name: "El venado",
    artist: "Frida Kahlo",
    description:
      "Su cabeza sigue siendo humana y mira directamente al espectador con una mezcla de vulnerabilidad y fuerza. Su expresión facial es pensativa y transmite emociones complejas. Las flechas, que recuerdan a las utilizadas en la caza, simbolizan el dolor, el sufrimiento y la vulnerabilidad.",
    url: "https://howsafeismexico.com/wp-content/uploads/2023/11/Untitled-design-25.webp",
    alt: "«El venado» representa a Frida Kahlo como un ciervo herido en un paisaje árido. La parte superior del cuerpo de Kahlo se transforma en el torso de un ciervo, con pelaje y pezuñas.",
  },
  {
    name: "La Espina",
    artist: "Raúl Anguiano",
    description:
      "El tapatío forma parte de la segunda generación de muralistas de México y fue uno de los fundadores del Taller de la Gráfica popular. Su obra se considera realista y toca temas socio-políticos. La Espina es una obra que se remonta a una memoria personal en donde recuerda un viaje que hizo a Bonampak en la selva Lacandona y María, su guía, se clavó una espina.",
    url: "https://img.travesiasdigital.com/2023/08/05-interna-10-obras-del-Museo-de-Arte-Moderno-graciela-iturbide-1024x738.jpg",
    alt: "Durante este viaje solo fotografió a mujeres de la comunidad.",
  },
  {
    name: "Los jugadores de cartas",
    artist: "Paul Cézanne",
    description:
      "Los jugadores de cartas es una serie de cinco pinturas que representan a trabajadores rurales jugando a las cartas. Cézanne exploró la geometría y la estructura en esta obra.",
    url: "https://historia-arte.com/_/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpbSI6WyJcL2FydHdvcmtcL2ltYWdlRmlsZVwvY2FyZF9wbGF5ZXJzLXBhdWxfY2V6YW5uZS5qcGciLCJyZXNpemVDcm9wLDYwMCwzMDAsNTAlLDQwJSJdfQ.QxFMVxMStWn4ZGx0aetwRHoBxHEROFwq-hlrFSdh/vgA.jpg",
    alt: "Imagen de Los jugadores de cartas",
  },
  {
    name: "Guernica",
    artist: "Pablo Picasso",
    description:
      "Guernica es una poderosa representación del horror de la guerra y la brutalidad. Picasso pintó esta obra como respuesta al bombardeo de Guernica durante la Guerra Civil Española.",
    url: "https://static5.museoreinasofia.es/sites/default/files/obras/DE00050.jpg",
    alt: "Imagen de Guernica",
  },
  {
    name: "La persistencia de la memoria",
    artist: "Salvador Dalí",
    description:
      "La persistencia de la memoria presenta relojes derretidos en un paisaje surrealista. Dalí jugó con la relatividad del tiempo en esta icónica obra del surrealismo.",
    url: "https://historia-arte.com/_/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpbSI6WyJcL2FydHdvcmtcL2ltYWdlRmlsZVwvbnljLTItMjY4LmpwZyIsInJlc2l6ZSw4MDAiXX0.oHuiNXMeCqBT1fa5I9ddteiMiKkLQ_e1lyF6l8uUsWo.jpg",
    alt: "Imagen de La persistencia de la memoria",
  },
];
